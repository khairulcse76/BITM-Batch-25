<?php 
$Firstname=$lastName=$email=$web=$phoneNumber='';
    $errfastname=$errlastname=$erremail=$errphone ="";
   if($_SERVER["REQUEST_METHOD"]=="POST")
    {
        if(empty($_POST['firstName']))
        {
          $errfastname="*";
//          echo $errfastname="Name Filed Required";
        }else
        {
           $Firstname= validate($_POST['firstName']);
        }
        
         if(empty($_POST['lastName']))
        {
             $errlastname="*";
        }
        else
        {
           $lastName= validate($_POST['lastName']);
        }
         if(empty($_POST['email']))
        {
             $erremail="*";
        }elseif (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
         $erremail="Invalid Email Format";
    }
        else
        {
           $email= validate($_POST['email']);
        }
         if(empty($_POST['phoneNumber']))
        {
             $errphone="*";
        }
        else
        {
           $phoneNumber= validate($_POST['phoneNumber']);
        }

    }
    
     function validate($data)
    {
        $data=  trim($data);
        $data= stripcslashes($data);
        $data=  htmlspecialchars($data);
        return $data;
    }
?>
     
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" href="form.css" type="text/css"/>
    </head>
    <body>
        
        <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="POST">
       <table id="table">
                <tr>
                    <td colspan="4" id="header">Registration Form</td>
                    
                </tr>
                  <tr>
                    <td>First Name :</td>
                    <td><input type="text" name="firstName" placeholder="First Name">
                        <span style="color: #ff6666;"><?PHP echo $errfastname;?></span></td>
                    <td>Last Name :</td>
                    <td><input type="text" name="lastName" placeholder="Last Name">
                        <span style="color: #ff6666;"><?PHP echo $errlastname;?></span></td>
                </tr>
                <tr>
                    <td>E-mail :</td>
                    <td><input type="text" name="email" placeholder="khairulcse76@gmail.com">
                        <span style="color: #ff6666;"><?PHP echo $erremail;?></span></td>
                    <td>Phone Number :</td>
                    <td><input type="text"  name="phoneNumber" placeholder="+8801774460981">
                        <span style="color: #ff6666;"><?PHP echo $errphone;?></span></td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td colspan="3"><input type="text" name="web" placeholder="www.maxbagworld.com"></td>
                    
                </tr>
                
                 <tr>
                     <td>Gender</td>
                     <td><input type="radio" name="gender" value="Male"><b>Male</b>
                         <input type="radio" name="gender" value="Female"><b>Female</b>
                         <input type="radio" name="gender" value="Other"><b>Other</b>
                     </td>
                     <td style="float: right">City</td>
                     <td>
                         <select name="option"> 
                              <option>---Select---</option>
                             <option>Dhaka</option>
                              <option>Rajshahi</option>
                               <option>Khulna</option>
                                <option>Sylet</option>
                                 <option>Rangpur</option>
                         </select>
                     </td>
                    
                </tr>
                
                 <tr>
                     <td colspan="3"><input type="submit" name="submit" value="Submit" id="submit"></td>
                     
                    
                </tr>
            </table>
        </form>
    </body>
</html>

