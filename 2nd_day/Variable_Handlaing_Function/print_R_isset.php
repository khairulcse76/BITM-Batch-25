<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <div>
            <div style="float: left;">
                 <pre>
                <?php
                    echo "Print_R <br>";
                    $a=array('a'=> 'apple', 'b' => 'banana', 'c'=>array('x'=>array('khairul','Saiful','khairul','Saiful','khairul','Saiful', 'Zaman'),'y','z'));
                    print_r($a);
                ?></pre>

                <pre> 
                    <?php
                        $b = array ('m' => 'monkey', 'foo' => 'bar', 'x' => array ('x', 'y', 'z'));
                        $results = print_r($b, false); // $results now contains output from print_r
                    ?>
                </pre>
            </div>
             <div style="float: left;">
                <div>IsSet isset</div>
                <?php
                    $varrr='';
                    if(isset($varrr))
                    {
                        echo "This var is set so i will print. <br>";
                    }
                    $khairulislam="test <br>";
                    $Malekuzzaman="anothertest <br>";
                    echo "Khairulislam Var_dupm check = ";        
                    var_dump(isset($khairulislam));
                    echo "<br>Malekuzzaman Var_dupm check = ";  
                    var_dump(isset($Malekuzzaman));
                ?>
            </div>
             <div style="float: right;">
                <?php 
                    $issetarray=array('test' =>1, 'hello'=>NULL, 'pie'=>array('first'=>'second'));
                    
                    var_dump(isset($issetarray['test']));
                    echo "<br>";
                    var_dump(isset($issetarray['foo']));
                    echo "<br>";
                    var_dump(isset($issetarray['hello']));
                ?>
            </div>

        </div>
       
    </body>
</html>
