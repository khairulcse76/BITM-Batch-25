<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <div>
        <?php
            function destroy_foo()
            {
                global $foo;
                unset($foo);
            }
            $foo = 'bar';
            destroy_foo();
            echo $foo;
        ?></div>
        <div>
           <?php
                function destroy_2()
                {
                    unset($GLOBALS['khairul']);
                }
                $khairul="Somthing";
                destroy_2();
           ?>
        </div>
        <div>
            
            <pre><?php 
                $khairul=array (1, 2, array('a', 'c', 'd'));
                var_dump($khairul);
            ?></pre>
            
            <?php 
                $m=3.2;
                $n=45;
                $o='a';
                $p="a";
                var_dump($m,$n,$p);
            ?>
        </div>
    </body>
</html>
